Inverted pendulum
==================

**Enviroment requirements**

Linux distro with

*  NodeJs 12.0 or higher
*  npm
*  wget
*  grunt-cli (write: npm install -g grunt-cli)
*  bower (write: npm install -g bower)

**Note:**

*  Bower is configured to install to directory frontend/jslibs
*  When dlib version changes most likely so will Gruntfile and binding.gyp since version is in dlib's pathname.

**To setup the system write:**

```
npm install
grunt install
```

**To build addon write:**

```
grunt addon-build
```