#
# Creates an install package for the Inverted pendulum.
# 
# Only neccessary files are included in this package.

package_build_dir=package
version_file=version.txt

echo $package_build_dir
rm -rf ${package_build_dir}
mkdir ${package_build_dir}

# Only files needed to run the server
cp ../server.js ${package_build_dir}
cp -r ../frontend ${package_build_dir}
cp -r ../node_modules ${package_build_dir}

mkdir ${package_build_dir}/addon
mkdir ${package_build_dir}/addon/build
mkdir ${package_build_dir}/addon/build/Release
cp -r ../addon/build/Release/addon.node ${package_build_dir}/addon/build/Release/addon.node
         
# Version information
echo "created at:" >> ${package_build_dir}/${version_file}
date >> ${package_build_dir}/${version_file}
echo "git commit:" >> ${package_build_dir}/${version_file}
git rev-parse HEAD  >> ${package_build_dir}/${version_file}
echo "git branch:" >> ${package_build_dir}/${version_file}
git rev-parse --abbrev-ref HEAD >> ${package_build_dir}/${version_file}

tar -zcvf inverted_pendulum.tar.gz ${package_build_dir}
