//
// Updates the pendulum geometry based on the angle and position.
//

function InvertedPendulumGeometryModel(cart_, box_, bar_, scene_) {     
  var that = this;
  // static configuration
  that.configuration = {
    cart: cart_,
    box: box_,
    bar: bar_
  };

  // State variables
  that.origo = {
    x: 0,
    y: cart_.height/2,
    z: 0
  };
  that.pendulumAngle = Math.pi/2;

  // computes current geometry 
  function computeRuntimeGeometry(origo, pendulumAngle, configuration) {
    var returnValue = {};
    returnValue.cart = {
      x: origo.x,
      y: origo.y,
      z: origo.z,
      rotation: 0 // cart is never rotatated.
    };

    returnValue.box = {
      x: origo.x + configuration.bar.length*Math.cos(pendulumAngle),
      y: origo.y + configuration.bar.length*Math.sin(pendulumAngle),
      z: that.origo.z,
      rotation: pendulumAngle + Math.PI/2
    };

    returnValue.bar = {
      x: origo.x + configuration.bar.length*Math.cos(pendulumAngle)/2,
      y: origo.y + configuration.bar.length*Math.sin(pendulumAngle)/2,
      z: origo.z,
      rotation: pendulumAngle + Math.PI/2
    };

    return returnValue;
  }

  // Draws objects first time and 
  // returns necessary handles. 
  that.handles = function() { //configuration, runtimeGeometry, scene) {
    var returnValue = {};
    var cartGeometry = new THREE.BoxGeometry( that.configuration.cart.length, 
                                              that.configuration.cart.height, 
                                              that.configuration.cart.width);

    var cartMaterial = new THREE.MeshBasicMaterial({
      color: that.configuration.cart.color,
      map: THREE.ImageUtils.loadTexture(that.configuration.cart.texture) 
    }); 
    
    returnValue.cart = new THREE.Mesh(cartGeometry, cartMaterial);

    var boxGeometry = new THREE.BoxGeometry(  that.configuration.box.length, 
                                              that.configuration.box.height, 
                                              that.configuration.box.width); 
  
    var boxMaterial = new THREE.MeshBasicMaterial({
      color: that.configuration.box.color,
      map: THREE.ImageUtils.loadTexture(that.configuration.box.texture) 
    }); 
    
    returnValue.box = new THREE.Mesh(boxGeometry, boxMaterial);

    var barGeometry = new THREE.CylinderGeometry( that.configuration.bar.width, 
                                                  that.configuration.bar.width,
                                                  that.configuration.bar.length,
                                                  that.configuration.bar.segments);
    var barMaterial = new THREE.MeshBasicMaterial({
      color: that.configuration.bar.color,
      map: THREE.ImageUtils.loadTexture(that.configuration.bar.texture)
    });
    returnValue.bar = new THREE.Mesh(barGeometry, barMaterial);

    scene.add(returnValue.cart);
    scene.add(returnValue.box);
    scene.add(returnValue.bar);

    return returnValue;
  }();

  that.propagate = function() {
    var runtimeGeometry = computeRuntimeGeometry(that.origo, that.pendulumAngle, that.configuration); 

    that.handles.cart.position.setX(runtimeGeometry.cart.x);
    that.handles.cart.position.setY(runtimeGeometry.cart.y);
    that.handles.cart.position.setZ(runtimeGeometry.cart.z);

    that.handles.box.position.setX(runtimeGeometry.box.x);
    that.handles.box.position.setY(runtimeGeometry.box.y);
    that.handles.box.position.setZ(runtimeGeometry.box.z);
    that.handles.box.rotation.z = runtimeGeometry.box.rotation; // rotating in xy plane

    that.handles.bar.position.setX(runtimeGeometry.bar.x);
    that.handles.bar.position.setY(runtimeGeometry.bar.y);
    that.handles.bar.position.setZ(runtimeGeometry.bar.z);
    that.handles.bar.rotation.z = runtimeGeometry.bar.rotation;   
  };
          
  that.propagate();

  return {
    setPendulumRotation: function(angle) {
      that.pendulumAngle = angle;
    },
    setCenterX: function(x) {
      that.origo.x = x;
    },
    setCenterY: function(y) {
      that.origo.y = y;
    },
    setCenterZ: function(z) {
      that.origo.z = z;
    },
    update: function() {
      that.propagate();
    }
  };
}
