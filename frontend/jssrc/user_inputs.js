// Inputs from the user

var velocity = function() {
  var that = this;
  var x_ = 0;
  var epoch_ = 0; 
  var is_valid_ = false;

  var velocity_ = [0, 0, 0, 0, 0];
  var coefs_ = [1, 0.7, 0.5, 0.3, 0.1];

  // scaling the coefs
  var sum  = 0;
  console.log(coefs_);
  for (var ii=0; ii < coefs_.length; ii++) {
    sum += 0;
  }
  for (var ii=0; ii < coefs_.length; ii++) {
    coefs_[ii] += coefs_[ii]/sum;
  }

  return {
    update: function(x) {
      for (var ii = velocity_.length - 1 ; ii > 0; ii--) {
        velocity_[ii] = velocity_[ii-1];
      }
      velocity_[ii] = (x - x_) / (Date.now()/1000 - epoch_);

      epoch_ = Date.now()/1000;
      x_ = x;
      is_valid_ = true;
    },
    current: function() {
      return velocity_[0];
    },
    isValid: function() {
      return is_valid_;
    },
    setInvalid: function() {
      is_valid = false;
      for (var ii=0; ii < velocity_.length; ii++) {
        velocity_[ii] = 0;
      }
    }
  }
}();

document.addEventListener('mousedown', onDocumentMouseDown, false);
document.addEventListener('touchstart', onDocumentTouchStart, false);
document.addEventListener('touchmove', onDocumentTouchMove, false);

function onDocumentMouseDown(event) {
  event.preventDefault();
  document.addEventListener('mousemove', onDocumentMouseMove, false);
  document.addEventListener('mouseup', onDocumentMouseUp, false);
  document.addEventListener('mouseout', onDocumentMouseOut, false);

  velocity.update(event.clientX);
}

function onDocumentMouseMove(event) {
  if (velocity.isValid()) {
    velocity.update(event.clientX);
    rotNoise = velocity.current();
  } else {
    rotNoise = 0;
  }
}

function onDocumentMouseUp(event) {
  document.removeEventListener('mousemove', onDocumentMouseMove, false);
  document.removeEventListener('mouseup', onDocumentMouseUp, false);
  document.removeEventListener('mouseout', onDocumentMouseOut, false);

  velocity.setInvalid();
}

function onDocumentMouseOut(event) {
  document.removeEventListener('mousemove', onDocumentMouseMove, false);
  document.removeEventListener('mouseup', onDocumentMouseUp, false);
  document.removeEventListener('mouseout', onDocumentMouseOut, false);
}

function onDocumentTouchStart(event) {
  velocity.update(event.touches[0].pageX);
}

function onDocumentTouchEnd(event) {
  velocity.setInvalid();
}

function onDocumentTouchMove(event) {
  if (velocity.isValid()) {
    velocity.update(event.touches[0].pageX);
    rotNoise = velocity.current();
  } else {
    rotNoise = 0;
  }
}