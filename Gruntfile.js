module.exports = function(grunt) {
  grunt.initConfig({
    jshint: {
      files: ['Gruntfile.js', 'server.js', 'frontend/*.js', 'frontend/jssrc/*.js'],
      options: {
        globals: {
          jQuery: true
        }
      }
    },
    exec: {
      addon_build: {
        cmd: 'cd addon && node-gyp clean && node-gyp configure && node-gyp build'
      },
      frontend_install: {
        cmd: 'bower install'
      },
      addon_install: {
        cmd: 'wget http://dlib.net/files/dlib-18.17.tar.bz2 -P addon/3rdparty && cd addon/3rdparty && tar xvfj dlib-18.17.tar.bz2 && rm dlib-18.17.tar.bz2'
      },
      make_server_package: {
        cmd: 'cd deployment && chmod +x makeServerPackage.sh && ./makeServerPackage.sh'
      },
      make_ec2_package: {
        cmd: 'cd deployment && chmod +x makeEc2Package.sh && ./makeEc2Package.sh'
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-exec');

  grunt.registerTask('make-deployment-package', ['exec:make_server_package']);
  grunt.registerTask('make-ec2-package', ['exec:make_ec2_package']);
  grunt.registerTask('frontend-install', ['exec:frontend_install']);
  grunt.registerTask('addon-build', ['exec:addon_build']);
  grunt.registerTask('addon-install', ['exec:addon_install']);
  grunt.registerTask('default', ['jshint']);
  grunt.registerTask('install', ['exec:frontend_install', 'exec:addon_install', 'exec:addon_build']);
  
};