//
// Inverted pendulum server
//
var path = require('path');

var _ = require('underscore');
var express = require('express');

var app = express();

// It seems that 'express.static' handles 'current directory' a bit differently than 'require'.
var frontendPath = path.join(__dirname, '/frontend');
app.use(express.static(frontendPath));

console.log('Listening on port 8010');
app.listen(8010);

var WebSocketServer = require('ws').Server;
var wss = new WebSocketServer({ port: 8180 });

var numberOfConnections = 0;
console.log("Waiting for connections.");

wss.on('connection', function connection(ws) {
  numberOfConnections++;
  console.log('Websocket connection established. Number of connections: ' + numberOfConnections);
  
  ws.on('message', function incoming(message) {
    var parsedMessage = JSON.parse(message);
    if (parsedMessage.rotationalDisturbance) {
      if (obj) {
        if (_.isNumber(parsedMessage.rotationalDisturbance)) {
          obj.setRotationalDisturbance(scaledNoise(parsedMessage.rotationalDisturbance));
        } else {
          console.log('Invalid noise value: ' + parsedMessage.rotationalDisturbance);
        }
      }
    }
  });

  var state = {
    x: 0,
    rot: Math.PI/2
  };  
  
  var socketIsOpen = true;
  var intervalID = setInterval(function() {
    if (socketIsOpen) {
      state = obj.state(); 
      // If setInterval time is long enough (~30ms)
      // there is no need for try catch but
      // for higher update frequencies 
      // try-catch seems to be necessary
      try {
        ws.send(JSON.stringify(state));
      } catch(err) {
        console.log(err);
      }
    }
  }, 10);

  ws.on('close', function() {
    numberOfConnections--;
    console.log('Websocket connection lost. Number of connections: ' + numberOfConnections);
    clearInterval(intervalID);
    socketIsOpen = false;
    obj.destroy();
  });

  var createObject = require('./addon/build/Release/addon');
  var obj = createObject();
  obj.run();
});

// Scales user input to interval [-L, L]
function scaledNoise(noise) {
  var L = 0.2;
  return -(2*L/(1+Math.exp(-0.01*noise)) - L/2); 
}