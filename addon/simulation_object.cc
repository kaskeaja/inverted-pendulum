#include <node.h>
#include "simulation_object.h"
#include <cstddef>
#include <iostream>

using namespace v8;

Persistent<Function> SimulationObject::constructor;

SimulationObject::SimulationObject(double value): value_(value) {
  simulation_ = new Simulation;
}

SimulationObject::~SimulationObject() {
}

void SimulationObject::Init() {
  Isolate* isolate = Isolate::GetCurrent();

  // Prepare constructor template
  Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, New);
  tpl->SetClassName(String::NewFromUtf8(isolate, "SimulationObject"));
  tpl->InstanceTemplate()->SetInternalFieldCount(1);

  // Prototype
  NODE_SET_PROTOTYPE_METHOD(tpl, "init", Init);
  NODE_SET_PROTOTYPE_METHOD(tpl, "run", Run);
  NODE_SET_PROTOTYPE_METHOD(tpl, "state", State);
  NODE_SET_PROTOTYPE_METHOD(tpl, "destroy", Destroy);
  NODE_SET_PROTOTYPE_METHOD(tpl, "setRotationalDisturbance", SetRotationalDisturbance);

  constructor.Reset(isolate, tpl->GetFunction());
}

void SimulationObject::New(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  if (args.IsConstructCall()) {
    // Invoked as constructor: `new MyObject(...)`
    double value = args[0]->IsUndefined() ? 0 : args[0]->NumberValue();
    SimulationObject* obj = new SimulationObject(value);
    obj->Wrap(args.This());
    args.GetReturnValue().Set(args.This());
  } else {
    // Invoked as plain function `MyObject(...)`, turn into construct call.
    const int argc = 1;
    Local<Value> argv[argc] = { args[0] };
    Local<Function> cons = Local<Function>::New(isolate, constructor);
    args.GetReturnValue().Set(cons->NewInstance(argc, argv));
  }
}

void SimulationObject::NewInstance(const FunctionCallbackInfo<Value> &args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  const unsigned argc = 1;
  Handle<Value> argv[argc] = { args[0] };
  Local<Function> cons = Local<Function>::New(isolate, constructor);
  Local<Object> instance = cons->NewInstance(argc, argv);

  args.GetReturnValue().Set(instance);
}

void SimulationObject::Destroy(const FunctionCallbackInfo<Value> &args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  SimulationObject* obj = ObjectWrap::Unwrap<SimulationObject>(args.Holder());
  if (obj->simulation_ != nullptr) {
    delete obj->simulation_;
    obj->simulation_ = nullptr;
  }
}

void SimulationObject::Run(const v8::FunctionCallbackInfo<v8::Value> &args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  SimulationObject* obj = ObjectWrap::Unwrap<SimulationObject>(args.Holder());
  if (obj->simulation_ != nullptr) {
    obj->simulation_->run();
  }
}

void SimulationObject::Init(const v8::FunctionCallbackInfo<v8::Value> &args) {

  std::cout << "SimulationObject::Init()" << std::endl;

  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  SimulationObject* obj = ObjectWrap::Unwrap<SimulationObject>(args.Holder());
  if (obj->simulation_ != nullptr) {
    obj->simulation_->init();
  }
}

void SimulationObject::State(const v8::FunctionCallbackInfo<v8::Value> &args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  SimulationObject* obj = ObjectWrap::Unwrap<SimulationObject>(args.Holder());
  Local<Object> returnValue = Object::New(isolate);

  if (obj->simulation_ != nullptr) {
    Simulation::State state = obj->simulation_->state(); 
    returnValue->Set(String::NewFromUtf8(isolate, "x"), Number::New(isolate, state.x));
    returnValue->Set(String::NewFromUtf8(isolate, "rot"), Number::New(isolate, state.rot));
  } else {
    returnValue->Set(String::NewFromUtf8(isolate, "x"), Undefined(isolate));
    returnValue->Set(String::NewFromUtf8(isolate, "rot"), Undefined(isolate));
  }

  args.GetReturnValue().Set(returnValue);
}

void SimulationObject::SetRotationalDisturbance(const v8::FunctionCallbackInfo<v8::Value> &args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  SimulationObject* obj = ObjectWrap::Unwrap<SimulationObject>(args.Holder());

  if (obj->simulation_ != nullptr) {
    // Let's get check that input value is of the right type.
    if (args.Length() == 1) {
      if (args[0]->IsNumber()) {
        obj->simulation_->setRotationalDisturbance(args[0]->NumberValue());
      } else {
        isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(isolate, "The argument must be a number.")));
      }
    } else {
      isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(isolate, "Wrong number of arguments. There must be exactly one argument.")));
    }
  }
}