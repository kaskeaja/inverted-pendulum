#ifndef DIFFERENTIAL_EQUATION_H
#define DIFFERENTIAL_EQUATION_H

#include <assert.h>
#include <functional>
#include <iostream>
#include <vector>

namespace differentialEquation {
  class State {
  public:
    State(size_t dim) {
      data_.clear();
      for (size_t ii = 0; ii < dim; ii++) {
        data_.push_back(0);
      }
    }
  
    State& operator+(const State &s) {
      assert(this->data_.size() == s.data_.size());
      for (size_t ii = 0; ii < this->data_.size(); ii++) {
        this->data_.at(ii) = this->data_.at(ii) + s.data_.at(ii);
      } 
      return *this;
    }
  
    State operator*(double scalar) {
      for (size_t ii = 0; ii < this->data_.size(); ii++) {
        this->data_.at(ii) = this->data_.at(ii)*scalar;
      } 
      return *this;
    }

    State operator/(double scalar) {
      // will fail if scalar is zero
      for (size_t ii = 0; ii < this->data_.size(); ii++) {
        this->data_.at(ii) = this->data_.at(ii)/scalar;
      } 
      return *this; 
    }

    std::vector<double> toVector() {
      return data_;
    }
  
    double& at(size_t ii) {
      return this->data_.at(ii);
    } 
  
    const double& at(size_t ii) const{
      return this->data_.at(ii);
    }

    void print() {
      for (size_t ii = 0; ii < data_.size(); ii++) {
        std::cout << "ii:" << data_.at(ii) <<std::endl;
      } 
    }
  private:
    std::vector<double> data_;
  };

  class RK4 {
  public:
    RK4(double stepsize, State initialState, const std::function <State(double, State&, double, double)> &propagator);
    State& propagate(double &tn, double &control, double &rotatationalDisturbance);
  private:
    double stepsize_;
    State state_;
    const std::function <State(double, State&, double, double)> &propagator_;
    double tn;
  };
}
#endif // DIFFERENTIAL_EQUATION_H