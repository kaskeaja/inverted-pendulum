#include "differential_equation.h"
#include <iostream>

using namespace differentialEquation;

RK4::RK4(double stepsize, State initialState, const std::function <State(double, State&, double, double)> &propagator):
  stepsize_(stepsize), 
  state_(initialState),
  propagator_(propagator) {
}

State& RK4::propagate(double &tn, double &control, double &rotatationalDisturbance) {
  State k1 = propagator_(tn, state_, control, rotatationalDisturbance);
  State k2 = propagator_(tn + stepsize_/2.0, state_ + k1*stepsize_/2.0, control, rotatationalDisturbance);
  State k3 = propagator_(tn + stepsize_/2.0, state_ + k2*stepsize_/2.0, control, rotatationalDisturbance);
  State k4 = propagator_(tn + stepsize_, state_ +  k3*stepsize_, control, rotatationalDisturbance);
  tn = tn + stepsize_;
  state_ = state_ + (k1 + k2*2.0 + k3*2.0 + k4)*stepsize_/6.0;
  return state_;
}