#include "simulation.h"
#include "helpers.h"
#include <dlib/optimization.h> // Needed for MPC
#include <dlib/matrix.h>
#include <assert.h>
#include <cmath>
#include <chrono>
#include <functional>
#include <iostream>
#include <mutex>
#include <vector>

// Mathematical constants
const double PI = 3.141592653589793238463;
const double g = 9.81;

// System properties
const double m = 0.1;                     // Pendulum mass
const double M = 1;                       // Cart mass
const double L = 0.5;                     // Pendulum length
const double rotationFrictionCoef = 0.01; // Effective coefficient of friction in the hinge connecting cart to pendulum.
const double linearFrictionCoef = 0.1;    // Effective coefficient of friction between the cart and the ground.
const int predictionHorizon = 20;

// Simulation properties
const int predictionTimeStep = 50;    // in ms 
const int simulationTimeStep = 1;     // in ms

// Measurement noise levels
const double dphidt_noise_std = 0.01;
const double phi_noise_std    = 0.015;
const double dxdt_noise_std   = 0.1;
const double x_noise_std      = 0.2;

std::mutex setupMutex; // Makes sure that controller is ready when the simulation starts.

// Simulation thread.
void iterateSimulation(Simulation::State &state, double &control, double &rotationalDisturbance, Simulation::SimulationRunState &run_state) {
  std::chrono::microseconds delta_t(1000*simulationTimeStep);
  Simulation::State localState = state; // should create a copy

  // Setup actual simulation here.
  differentialEquation::State propagator_state(4);
  double tn = 0;

  // state init 
  propagator_state.at(0) = 0;                     // d(phi)/dt
  propagator_state.at(1) = PI/double(2.0);        // phi
  propagator_state.at(2) = 0;                     // d(x)/dt
  propagator_state.at(3) = 0;                     // x

  differentialEquation::RK4 propagator(static_cast<double>(simulationTimeStep)*0.001, propagator_state,
    []( double t, 
    differentialEquation::State state_n, 
    double control, 
    double rotationalDisturbance)->differentialEquation::State {
      differentialEquation::State state_next(4);

      double F_linear = control - linearFrictionCoef*state_n.at(2); 
      double F_rotation = rotationalDisturbance - rotationFrictionCoef*state_n.at(0);

      state_next.at(0) =  (F_linear*sin(state_n.at(1)) - (m + M)*g*cos(state_n.at(1))
                          + m*L*state_n.at(0)*state_n.at(0)*cos(state_n.at(1))*sin(state_n.at(1))
                          + F_rotation*(m+M)) / 
                          (L*(m + M - m*sin(state_n.at(1))*sin(state_n.at(1))));
      
      state_next.at(1) = state_n.at(0);
      
      state_next.at(2) =  (F_linear + F_rotation*m*sin(state_n.at(1))
                          - m*g*sin(state_n.at(1))*cos(state_n.at(1)) 
                          + m*L*state_n.at(0)*state_n.at(0)*cos(state_n.at(1))) / 
                          (m + M - m*sin(state_n.at(1))*sin(state_n.at(1)));

      state_next.at(3) = state_n.at(2);
    return state_next;
  });
  
  // Making sure the controller is properly set up before simulation starts.
  setupMutex.lock();
  setupMutex.unlock();
  auto start = std::chrono::system_clock::now();
  auto end = std::chrono::system_clock::now();
  while (run_state != Simulation::SimulationRunState::terminated) {
    start = std::chrono::system_clock::now();

    if (run_state == Simulation::SimulationRunState::running) {
      differentialEquation::State yn = propagator.propagate(tn, control, rotationalDisturbance);
      
      localState.drot_dt = yn.at(0);
      localState.rot = yn.at(1);
      
      localState.dx_dt = yn.at(2);
      localState.x = yn.at(3);
    }
    
    state = localState;

    end = std::chrono::system_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
    
    if (duration > delta_t) {
      std::cout << " Warning: Simulation step takes too long: " << duration.count() << " ms > " << delta_t.count() << " ms" << std::endl;
    } else {
      std::this_thread::sleep_for(delta_t - duration);
    }
    std::this_thread::sleep_for(delta_t);
  }
}

// Controller thread.
void iterateController(Simulation::State &state, double &control, Simulation::SimulationRunState &run_state) {
  std::chrono::microseconds delta_t(predictionTimeStep*1000); // [predictionTimeStep] = ms

  int const stateSpaceDim = 4;
  int const controlSpaceDim = 1;

  // Even linear MPC requires a quit bit of setting up.

  dlib::matrix<double, stateSpaceDim, stateSpaceDim>    Acont;
  dlib::matrix<double, stateSpaceDim, controlSpaceDim>  Bcont;

  // System is linearized with respect to x0 = [0, PI/2, 0, 0], the upright position,
  // leading to a system of the form d(x-x0)/dt = A(x-x0) + Bu.

  // Row 1
  Acont(0,0) = -rotationFrictionCoef*(m+M)/(L*M);
  Acont(0,1) = (m+M)*g/(L*M);
  Acont(0,2) = -linearFrictionCoef/(L*M);
  Acont(0,3) = 0;

  // Row 2  
  Acont(1,0) = 1;
  Acont(1,1) = 0;
  Acont(1,2) = 0;
  Acont(1,3) = 0;
  
  // Row 3
  Acont(2,0) = -rotationFrictionCoef*m/M;
  Acont(2,1) = m*g/M;
  Acont(2,2) = -linearFrictionCoef/M;
  Acont(2,3) = 0;
  
  // Row 4
  Acont(3,0) = 0;
  Acont(3,1) = 0;
  Acont(3,2) = 1;
  Acont(3,3) = 0;
  
  Bcont(0,0) = 1/(L*M); 
  Bcont(0,1) = 0;
  Bcont(0,2) = 1/M;
  Bcont(0,3) = 0;
  
  assert(stateSpaceDim == Acont.nr());
  assert(stateSpaceDim == Acont.nc());
  assert(stateSpaceDim == Bcont.nr());
  assert(controlSpaceDim == Bcont.nc());

  dlib::matrix<double, stateSpaceDim, stateSpaceDim>    A;
  dlib::matrix<double, stateSpaceDim, controlSpaceDim>  B;

  // needed since otherwise Acon*... is casted to const => error inside matrixExponent(...)
  dlib::matrix<double, stateSpaceDim, stateSpaceDim>    tempMatrix = Acont*predictionTimeStep*0.001; 
  A = matrixExponential(tempMatrix);
  
  // Acont might be (and actually is) singular matrix and therefore
  // computing int(e^(At)*B, 0, T) is a little bit tricky thing to do.
  B = matrixExponentialIntegrated(A, predictionTimeStep*0.001)*Bcont;

  // Implied matrices.
  dlib::matrix<double, predictionHorizon*stateSpaceDim,   predictionHorizon*controlSpaceDim>  C;
  dlib::matrix<double, predictionHorizon*controlSpaceDim, predictionHorizon*controlSpaceDim>  H;
  dlib::matrix<double, stateSpaceDim,                     stateSpaceDim>                      G;
  dlib::matrix<double, predictionHorizon*controlSpaceDim, stateSpaceDim>                      F;
  dlib::matrix<double, predictionHorizon*controlSpaceDim, predictionHorizon*stateSpaceDim>    L;
  dlib::matrix<double, predictionHorizon*stateSpaceDim,   stateSpaceDim>                      M;

  dlib::matrix<double, controlSpaceDim,                   controlSpaceDim>                    R;
  dlib::matrix<double, stateSpaceDim,                     stateSpaceDim>                      Q;
  dlib::matrix<double, stateSpaceDim,                     stateSpaceDim>                      Qfinal;

  dlib::matrix<double, predictionHorizon*controlSpaceDim, predictionHorizon*controlSpaceDim>  Rbar;
  dlib::matrix<double, predictionHorizon*stateSpaceDim,   predictionHorizon*stateSpaceDim>    Qbar;
  
  setToZero(C); 
  setToZero(H);
  setToZero(G);
  setToZero(F);
  setToZero(M);
  setToZero(R);
  setToZero(Q);
  setToZero(Qfinal);
  setToZero(Rbar);
  setToZero(Qbar);
  
  // Filling implied matrices.
  dlib::matrix<double, stateSpaceDim, stateSpaceDim> Atemp;
  setToIdentity(Atemp);
  
  // Costs for errors
  Q(0,0) = 2;
  Q(1,1) = 5;
  Q(2,2) = 1;
  Q(3,3) = 1.5;

  Qfinal = 10*Q; // Lets make sure the end state cost is significant.

  // Lets make control input cost small compared to state error cost.
  R(0,0) = 0.1;
  
  blockAssignment(M, A, 0, 0);
  for (int ii = 0; ii < predictionHorizon; ii++) {
    Atemp = Atemp*A;
    blockAssignment(M, Atemp, ii*A.nr(), 0);
  }
  
  // Filling the C matrix is the hardest part. However, it does have internal structure that one can use.

  // Lets fill the matrix along the (block) diagonals starting from the diagonal spanning from upper
  // left corner to the right lower corner. Next iterations will fill successive (block) diagonals towards
  // the lower left corner.

  dlib::matrix<double, stateSpaceDim, controlSpaceDim> AnB = B;

  // Number of block (sub) diagonals equals prediction horizon.
  for (int diag = 0; diag < predictionHorizon; diag++) {
    int rowOffset = diag*stateSpaceDim;
    int colOffset = 0 ; // Always starts from the left edge of the matrix.
    for (int ii = 0; ii < (predictionHorizon-diag); ii++) {
      blockAssignment(C, AnB, rowOffset + ii*stateSpaceDim, colOffset + ii*controlSpaceDim);
    }
    // Only matrix B goes to the main block diagonal and therefore
    // must increment AnB matrix only after the assignment.
    AnB = A*AnB;
  }

  for (int ii = 0; ii<predictionHorizon; ii++) {
    if( ii != (predictionHorizon-1)) {
      blockAssignment(Qbar, Q, ii*stateSpaceDim, ii*stateSpaceDim);
    } else {
      blockAssignment(Qbar, Qfinal, ii*stateSpaceDim, ii*stateSpaceDim);
    }
    blockAssignment(Rbar, R, ii*controlSpaceDim, ii*controlSpaceDim);
  }
  
  H = dlib::trans(C)*Qbar*C + Rbar;
  G = dlib::trans(M)*Qbar*M + Q;
  F = dlib::trans(C)*Qbar*M;

  // Box constraints.
  double uMin = -10; 
  double uMax = 10;

  // Best guess for system state.
  dlib::matrix<double, stateSpaceDim, 1> x;
  setToZero(x);

  // Optimized control input.
  dlib::matrix<double, predictionHorizon*controlSpaceDim, 1> u;
  setToZero(u);

  // Reference value or control set point.
  dlib::matrix<double, predictionHorizon*stateSpaceDim, 1> r;
  
  // Random numbers for the noise
  dlib::rand noise; 

  setupMutex.unlock(); 
  
  auto start = std::chrono::system_clock::now();
  while (run_state != Simulation::SimulationRunState::terminated) {
    start = std::chrono::system_clock::now();
    
    // Lets input current system state assuming all parameters can be measured directly. 
    x(0) = state.drot_dt + noise.get_random_gaussian()*dphidt_noise_std;
    x(1) = state.rot - 0.5*PI + noise.get_random_gaussian()*phi_noise_std;
    x(2) = state.x + noise.get_random_gaussian()*dxdt_noise_std;
    x(3) = state.dx_dt + noise.get_random_gaussian()*x_noise_std;
    
    // MPC controller's optimization part.
    dlib::find_min_box_constrained( dlib::lbfgs_search_strategy(10),
                                    dlib::objective_delta_stop_strategy(1e-9)/*.be_verbose()*/,
                                    [&H, &F, &L, &G, &x, &r](const dlib::matrix<double, controlSpaceDim*predictionHorizon, 1> &uu) -> double {
                                      // Evaluates to 1x1 matrix whose value is retrieved and returned.
                                      return (dlib::trans(uu)*H*uu 
                                          + 2*dlib::trans(x)*dlib::trans(F)*uu 
                                          + dlib::trans(x)*G*x)(0);
                                    },
                                    // Apparently dlib uses column convention for derivatives.
                                    [&H, &F, &L, &G, &x, &r](const dlib::matrix<double, controlSpaceDim*predictionHorizon, 1> &uu) -> dlib::matrix<double, predictionHorizon*controlSpaceDim, 1> {
                                      return  dlib::trans(2*dlib::trans(uu)*H 
                                      + 2*dlib::trans(x)*dlib::trans(F));
                                    },
                                    u,
                                    uMin,
                                    uMax);

    
    // Checks how long it took to solve the control problem.
    auto end = std::chrono::system_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);

    // In model predictive control only first step is used as a control input.
    control = u(0);
    
    // If it took longer than delta_t then controller is too slow for the control cycle.
    if (duration > delta_t) {
      std::cout << " Warning: Controller can't keep up with the simulation: " << duration.count() << " ms > " << delta_t.count() << " ms." << std::endl;
    } else {
      std::this_thread::sleep_for(delta_t - duration);
    }
  }
}


// Simulation member functions

Simulation::Simulation():simulation_propagation_state(4) {
  run_state_ = running;
  initState();
}

Simulation::~Simulation() {
  run_state_ = terminated;
  simulation_.join();
  controller_.join();
}

void Simulation::run() {
  setupMutex.lock();
  simulation_ = std::thread(iterateSimulation, std::ref(state_), std::ref(control_), std::ref(rotationalDisturbance_), std::ref(run_state_));
  controller_ = std::thread(iterateController, std::ref(state_), std::ref(control_), std::ref(run_state_));
}

void Simulation::initState() {
  state_.x = 0;
  state_.dx_dt = 0;
  state_.rot = PI/2;
  state_.drot_dt = 0;

  control_ = 0;
  rotationalDisturbance_ = 0;
} 

Simulation::State Simulation::state() {
  return state_;
}

bool Simulation::setRotationalDisturbance(double rotDist) {
  // Assumes that disturbance amplitude check is done by the caller.
  rotationalDisturbance_ = rotDist;
  return true;
}
