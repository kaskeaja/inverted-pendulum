{
  "targets": [
    {
      "target_name": "addon",
      "sources": [ 
      	"addon.cc", 
      	"simulation_object.cc", 
      	"simulation.cc", 
      	"differential_equation.cc"
      ],
      "cflags": [
      	"-std=c++11", 
      	"-lpthread", 
      	"-lX11",
      	"-fexceptions"
      ],
      'cflags!': ['-fno-exceptions'],
      'cflags_cc!': ['-fno-exceptions', '-fno-rtti'],
      "include_dirs": ['3rdparty/dlib-18.17']
    }
  ],
}