#include "simulation_object.h"
#include <node.h>
#include <iostream>

using namespace v8;

void CreateObject(const FunctionCallbackInfo<Value> &args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);
  SimulationObject::NewInstance(args);
}

void InitAll(Handle<Object> exports, Handle<Object> module) {
  SimulationObject::Init();

  NODE_SET_METHOD(module, "exports", CreateObject);
}

NODE_MODULE(addon, InitAll)