#ifndef SIMULATION_H
#define SIMULATION_H

#include "differential_equation.h"
#include <thread>

class Simulation {
public:
  struct State {
    double x;
    double dx_dt;
    double rot;
    double drot_dt;
  };

  Simulation();
  ~Simulation();
  void run(); // Starts running the simulation.
  void initState();
  void init();
  bool setRotationalDisturbance(double rotDist);
  State state();
  enum SimulationRunState { running, terminated };
private:
  State state_; // Geometric state of the pendulum.
  differentialEquation::State simulation_propagation_state;
  double control_;
  double rotationalDisturbance_;
  SimulationRunState run_state_;

  std::thread controller_;
  std::thread simulation_;  
};
#endif