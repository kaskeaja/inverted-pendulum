// NodeJs interface and separation from actual simulation implementation.

#ifndef SIMULATION_OBJECT_H
#define SIMULATION_OBJECT_H

#include "simulation.h"
#include <node.h>
#include <node_object_wrap.h>

class SimulationObject : public node::ObjectWrap {
 public:
  static void Init();
  static void NewInstance(const v8::FunctionCallbackInfo<v8::Value> &args);

 private:
  explicit SimulationObject(double value = 0);
  ~SimulationObject();

  static void New(const v8::FunctionCallbackInfo<v8::Value> &args);
  static void Init(const v8::FunctionCallbackInfo<v8::Value> &args);
  static void Run(const v8::FunctionCallbackInfo<v8::Value> &args);
  static void Destroy(const v8::FunctionCallbackInfo<v8::Value> &args);
  static void State(const v8::FunctionCallbackInfo<v8::Value> &args);
  static v8::Persistent<v8::Function> constructor;
  static void SetRotationalDisturbance(const v8::FunctionCallbackInfo<v8::Value> &args);
  double value_;
  
  Simulation *simulation_;
};

#endif