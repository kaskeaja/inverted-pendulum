//
// Various helpers.
//

#include <assert.h>
#include <stdexcept>
#include <iostream>

// Returns sign of a number type.
template <class type> type sgn(const type &x) {
  if (x > 0) {
    return type(1);
  }
  return type(-1);
}

// Sets matrix to zero.
template <class type>
void setToZero(type &matrix) {
  for (int row = 0; row < matrix.nr(); row++) {
    for (int col = 0; col < matrix.nc(); col++) {
      matrix(row, col) = 0;
    }
  }
}

// Sets matrix to zero.
template <class type>
void setToIdentity(type &matrix) {
  setToZero(matrix);
  for (int ii = 0; ii < matrix.nr(); ii++) {
    matrix(ii, ii) = 1;
  }
}

// Used for block assignment of a matrix into an another matrix.
// Sets assigns the small matrix into the big matrix so that upper left corner of
// the small matrix is at position (upperCornelRow, upperCornerCol).
template <class type1, class type2> 
void blockAssignment(type1 &bigMatrix, const type2 &smallMatrix, const int &upperCornerRow, const int &upperCornerCol) {
  // Lets check that small matrix fits into the big matrix
  if ((upperCornerRow + smallMatrix.nr()) > bigMatrix.nr()) {
    throw std::out_of_range("Small matrix doesn't fit into a big matrix row-wise.");  
  }
  
  if ((upperCornerCol + smallMatrix.nc()) > bigMatrix.nc()) {
    throw std::out_of_range("Small matrix doesn't fit into a big matrix column-wise."); 
  }

  // Guaranteed to fit
  for (int row = 0; row < smallMatrix.nr(); row++) {
    for (int col = 0; col < smallMatrix.nc(); col++) {
      bigMatrix(row + upperCornerRow, col + upperCornerCol) = smallMatrix(row, col);
    }
  }
}

// Computes definite integral of exp(At) from zero to delta_t.
template <class type>
type matrixExponentialIntegrated(const type &matrix, const double &delta_t) {
  assert(delta_t < 0.1); // delta_t must be 'small' to make sure power series converge

  auto result = matrix;
  auto matrixToPower = matrix;

  setToZero(result);
  setToIdentity(matrixToPower); // starts with matrix^0

  long kFactorial = 1;
  result = matrixToPower;
  
  int K = 40; // Should be enough. Some other criteria, i.e. relative change, could be used also.
  for (int ii = 1; ii < K; ii++) {
    kFactorial *= (ii+1);
    matrixToPower *= delta_t*matrix;
    result += matrixToPower*(1/static_cast<double>(kFactorial));
  }
  return result*delta_t;
}

template <class type>
type matrixExponential(const type &matrix) {
  auto result = matrix;
  auto matrixToPower = matrix;

  setToZero(result);
  setToIdentity(matrixToPower); // starts with matrix^0

  long kFactorial = 1;
  result = matrixToPower;
  
  int K = 20; // Should be enough. Sme other criteria, i.e. relative change, could be used also.
  for (int ii = 1; ii < K; ii++) {
    kFactorial *= ii;
    matrixToPower *= matrix;
    result += matrixToPower*(1/static_cast<double>(kFactorial));
  }
  return result;
}

template <class type>
void printMatrixCSV(const type &matrix) {
  for (auto ii = 0; ii < matrix.nr(); ii++) {
    for (auto jj = 0; jj < matrix.nc(); jj++) {
      std::cout << matrix(ii,jj);
      if (jj == (matrix.nc()-1)) {
        std::cout << std::endl;       
      } else {
        std::cout << ", ";
      }
    }
  }
}